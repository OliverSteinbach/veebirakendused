window.onload = initLinks;

var myPix = new Array("images/pathfinder.gif", "images/surveyor.gif", "images/surveyor98.igf");
var thisPic = 0;

function initLinks() {
	document.getElementById("prevLink").onclick = proccessPrevious;
	document.getElementById("nextLink").onclick = proccessNext;
}

function processPrevious() {
	if(thisPic == 0) {
		thisPic = myPix.length;
	}
	thisPic--;
	document.getElementById("myPicture").src = myPix[thisPic];
	return false;
}

function processNext() {
	thisPic++;
	if(thisPic == myPix.length) {
		thisPic = 0;
	}
	document.getElementById("myPicture").src = myPix[thisPic];
	return false;
}