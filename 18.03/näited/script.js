// JavaScript Document
document.onkeydown = keyHit;
var thisPic = 0;
var myPix = new Array("images/callisto.jpg", "images/europa.jpg", "images/io.jpg", "images/ganymede.jpg");
var imgCt = myPix.length-1;
var ltArrow = 37;
var rtArrow = 39;

function keyHit(evt) {
	var thisKey = (evt) ? evt.which : window.event.keyCode;
	
	if(thisKey == ltArrow) {
		chgSlide(-1);
	}
	else if(thisKey == rtArrow) {
		chgSlide(1);
	}
	return false;
}

function chgSlide(direction) {
	thisPic = thisPic + direction;
	if(thisPic > imgCt) {
		thisPic = 0;
	}
	if(thisPic < 0) {
		thisPic = imgCt;
	}
	document.getElementById("myPicture").src = myPix[thisPic];
}

window.onload = init;

function init() {
	choosePic();
	initLinks();
}

var myPix2 = new Array("images/lion.jpg", "images/tiger.jpg", "images/bear.jpg");

function choosePic() {
	var randomNum = Math.floor((Math.random() * myPix2.length));
	document.getElementById("myPicture2").src = myPix2[randomNum];     
}

var myPix3 = new Array("images/pathfinder.gif", "images/surveyor.gif", "images/surveyor98.gif");
var thisPic2 = 0;

function initLinks() {
	document.getElementById("prevLink").onclick = processPrevious;
	document.getElementById("nextLink").onclick = processNext;
}

function processPrevious() {
	if(thisPic2 == 0) {
		thisPic2 = myPix3.length;
	}
	thisPic2--;
	document.getElementById("myPicture3").src = myPix3[thisPic2];
	return false;
}

function processNext() {
	thisPic2++;
	if(thisPic2 == myPix3.length) {
		thisPic2 = 0;
	}
	document.getElementById("myPicture3").src = myPix3[thisPic2];
	return false;
}