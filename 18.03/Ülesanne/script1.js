document.onkeydown = keyHit;
var thisPic = 0;

var captionText = new Array(
    "Callisto", "Europa", "Ganymede", "Io"    
)

 function keyHit(evt) {
     document.getElementById("imgText1").innerHTML = captionText[0];
     var myPix = new Array("images/callisto.jpg", "images/europa.jpg", "images/io.jpg", "images/ganymede.jpg");
     var imgCt = myPix.length-1;
     var ltArrow = 37;
     var rtArrow = 39;

     var thisKey = (evt) ? evt.which : window.keyCode;

     if(thisKey == ltArrow) {
        chgSlide(-1);
     }
     else if (thisKey == rtArrow) {
        chgSlide(1);        
     }
     return false;

     function chgSlide(direction) {
         //var imgCt = captionText.length;
         thisPic = thisPic + direction;
         if (thisPic > imgCt) {
             thisPic = 0;
         }
         if (thisPic < 0) {
            thisPic = imgCt;
        }
        document.getElementById("myPicture").src = myPix[thisPic];
        document.getElementById("imgText1").innerHTML = captionText[thisPic];
     }
 }