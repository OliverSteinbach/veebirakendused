window.onload = initAll;

var currImg = 0;
var captionText = new Array(
    "Pilt 1", "Pilt 2", "Pilt 3"    
)
function initAll() {
    document.getElementById("imgText2").innerHTML = captionText[0];
    document.getElementById("prevLink").onclick = processPrevious;
    document.getElementById("nextLink").onclick = processNext;
}

function processPrevious() {
    newSlide(-1);
}

function processNext() {
    newSlide(1);
}

function newSlide(direction) {
    var imgCt = captionText.length;
    currImg = currImg + direction;
    if (currImg < 0) {
        currImg = imgCt-1;
    }
    if (currImg == imgCt) {
        currImg = 0;
    }
    document.getElementById("slideshow").src = "images/pilt" + currImg + ".jpg";
    document.getElementById("imgText2").innerHTML = captionText[currImg];
}