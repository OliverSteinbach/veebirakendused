window.onload = initAll;

var currImg = 0;
var captionText = new Array(
    "Text 1", "Text 2", "Text 3", 
    "Text 4", "Text 5", "Text 6",
    "Text 7", "Text 8", "Text 9",
    "Text 10",
)
function initAll() {
    document.getElementById("imgText").innerHTML = captionText[0];
    document.getElementById("prevLink").onclick = processPrevious;
    document.getElementById("nextLink").onclick = processNext;
}

function processPrevious() {
    newSlide(-1);
}

function processNext() {
    newSlide(1);
}

function newSlide(direction) {
    var imgCt = captionText.length;
    currImg = currImg + direction;
    if (currImg < 0) {
        currImg = imgCt-1;
    }
    if (currImg == imgCt) {
        currImg = 0;
    }
    document.getElementById("slideshow").src = "images/slideImg" + currImg + ".jpg";
    document.getElementById("imgText").innerHTML = captionText[currImg];
}