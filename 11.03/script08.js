// JavaScript Document
window.onLoad = initAll;

function initAll() {
	document.getElementById("redirect").onclick = 
		initRedirect;
}

function initRedirect() {
	alert("You are not responsible for the content of pages outside our site");
	window.location = this;
	return false;
}