window.onload = initAll;

document.onkeydown = keyHit;
var thisPic = 0;

var captionText1 = new Array(
    "Callisto", "Europa", "Io", "Ganymede"    
)

 function keyHit(evt) {
     document.getElementById("imgText1").innerHTML = captionText[0];
     var myPix = new Array("images/callisto.jpg", "images/europa.jpg", "images/io.jpg", "images/ganymede.jpg");     
     var ltArrow = 37;
     var rtArrow = 39;

     var thisKey = (evt) ? evt.which : window.keyCode;

     if(thisKey == ltArrow) {
        chgSlide(-1);
     }
     else if (thisKey == rtArrow) {
        chgSlide(1);        
     }    

     function chgSlide(direction) {
         var imgCt = captionText.length;
         thisPic = thisPic + direction;        
         if (thisPic > imgCt) {
             thisPic = 0;
         }
         if (thisPic < 0) {
            thisPic = imgCt;
         }
        document.getElementById("myPicture").src = myPix[thisPic];
        document.getElementById("imgText1").innerHTML = captionText1[thisPic];
     }
 }

var currImg = 0;
var captionText = new Array(
    "Pilt 1", "Pilt 2", "Pilt 3"    
)

function initAll() {
    document.getElementById("imgText2").innerHTML = captionText[0];
    document.getElementById("prevLink").onclick = processPrevious;
    document.getElementById("nextLink").onclick = processNext;
    choosePic();
}

function processPrevious() {
    newSlide(-1);
}

function processNext() {
    newSlide(1);
}

function newSlide(direction) {
    var imgCt = captionText.length;
    currImg = currImg + direction;
    if (currImg < 0) {
        currImg = imgCt-1;
    }
    if (currImg == imgCt) {
        currImg = 0;
    }
    document.getElementById("slideshow").src = "images/pilt" + currImg + ".jpg";
    document.getElementById("imgText2").innerHTML = captionText[currImg];
}

var currImg = 0;
var captionText3 = new Array("Reading1", "Reading2", "Reading3");
var myPix3 = new Array("images/reading1.gif", "images/reading2.gif", "images/reading3.gif");

function choosePic() {
	var randomNum = Math.floor((Math.random() * myPix3.length));
    document.getElementById("reading").src = myPix3[randomNum];     
    document.getElementById("imgText3").innerHTML = captionText3[currImg];
}